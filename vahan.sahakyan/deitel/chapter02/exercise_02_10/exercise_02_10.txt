2.10 State which of the following are true and which are false. If false, explain your answers. 


a. C++ operators are evaluated from left to right.
b. The following are all valid variable names: _under_bar_, m928134, t5, j7, her_sales, his_account_total, a, b, c, z, z2.
c. The statement cout << "a = 5;"; is a typical example of an assignment statement.
d. A valid C++ arithmetic expression with no parentheses is evaluated from left to right.
e. The following are all invalid variable names: 3g, 87, 67h2, h22, 2h.


Answers:


a.False: Only if all the operators are the same type.
b.True: BUT starting with "_"  is not recommended, because sometimes the compiler may misunderstand its purpose.
c.False: it's an output print text
d.False: FIRSTLY multiplication(*), division(/), modular(%). SECONDLY addition(+), subtraction(-)
e.False: h22 is valid. In C++ variables start with "_" symbol or a letter.
