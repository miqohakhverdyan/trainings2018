a. Comments are used to document a program and improve its readability.
b. The object used to print information on the screen is cout.
c. A C++ statement that makes a decision is if.
d. Most calculations are normally performed by assignment statements.
e. The cin object inputs values from the keyboard.