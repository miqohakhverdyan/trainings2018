State the order of evaluation of the operators in each of the following C++ statements and show the value of x after each statement is performed.
a. x = 7 + 3 * 6 / 2 - 1 ;
b. x = 2 % 2 + 2 * 2 - 2 / 2 ;
c. x = ( 3 * 9 * ( 3 + ( 9 * 3 / ( 3 ) ) ) );

a. First:3 * 6 = 18, then 18 / 2, then 9 + 7, then 16 - 1. x = 15.
b. First:2 % 2 = 0, then 2 * 2 = 4, then 2 / 2 = 1, then 4 - 1 = 3.
c. First:3 / 3 = 1, 9 * 1 = 9, 3 + 9 = 12, 3 * 9 = 27, 27 * 12 = 324.
