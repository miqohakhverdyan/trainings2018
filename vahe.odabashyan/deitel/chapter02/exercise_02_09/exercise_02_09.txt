Write a single C++ statement or line that accomplishes each of the following:
 
1.Print the message "Enter two numbers". 
	std::cout << "Enter two numbers: ";
 
2.Assign the product of variables b and c to variable a.
	 a = b * c;

3.State that a program performs a payroll calculation (i.e., use text that helps to document a program).
	//Payroll calculation program

4.Input three integer values from the keyboard into integer variables a, b and c.
	std::cin >> a >> b >> c;
