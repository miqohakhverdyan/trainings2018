State which of the following are true and which are false. If false, explain your answers. 
1.C++ operators are evaluated from left to right. 
FALSE, as the assignment operator associates from right to left.

2.The following are all valid variable names: _under_bar_, m928134, t5, j7, her_sales, his_account_total, a, b, c, z, z2. 
Overall this is TRUE, however, having an identifier that begins with an underscore
is not good programming practice.

3.The statement cout << "a = 5;"; is a typical example of an assignment statement.
FALSE, as it will simply print "a = 5;".

4.A valid C++ arithmetic expression with no parentheses is evaluated from left to right.
FALSE, it will be evaluated according to associativity and order of precedence.
	
5.The following are all invalid variable names: 3g, 87, 67h2, h22, 2h.
FALSE, in C++ variables cannot begin with a number, however, "h22" begins with
a letter and that is a  valid declaration.
