/// A program that prints the integer equivalent of a character

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    std::cout << "The integer equivalent of " << 'A' << " is " << static_cast<int>('A') << std::endl;
    std::cout << "The integer equivalent of " << 'B' << " is " << static_cast<int>('B') << std::endl;
    std::cout << "The integer equivalent of " << 'C' << " is " << static_cast<int>('C') << std::endl;
    std::cout << "The integer equivalent of " << 'D' << " is " << static_cast<int>('D') << std::endl;
    std::cout << "The integer equivalent of " << 'a' << " is " << static_cast<int>('a') << std::endl;
    std::cout << "The integer equivalent of " << 'b' << " is " << static_cast<int>('b') << std::endl;
    std::cout << "The integer equivalent of " << 'c' << " is " << static_cast<int>('c') << std::endl;
    std::cout << "The integer equivalent of " << 'd' << " is " << static_cast<int>('d') << std::endl;
    std::cout << "The integer equivalent of " << '0' << " is " << static_cast<int>('0') << std::endl;
    std::cout << "The integer equivalent of " << '1' << " is " << static_cast<int>('1') << std::endl;
    std::cout << "The integer equivalent of " << '2' << " is " << static_cast<int>('2') << std::endl;
    std::cout << "The integer equivalent of " << '3' << " is " << static_cast<int>('3') << std::endl;
    std::cout << "The integer equivalent of " << '!' << " is " << static_cast<int>('!') << std::endl;
    std::cout << "The integer equivalent of " << '@' << " is " << static_cast<int>('@') << std::endl;
    std::cout << "The integer equivalent of " << '#' << " is " << static_cast<int>('#') << std::endl;
    std::cout << "The integer equivalent of " << '#' << " is " << static_cast<int>('#') << std::endl;
    std::cout << "The integer equivalent of " << '$' << " is " << static_cast<int>('$') << std::endl;
    std::cout << "The integer equivalent of " << ' ' << " is " << static_cast<int>(' ') << std::endl;

    return 0; /// the program completed successfully
} /// end function main

