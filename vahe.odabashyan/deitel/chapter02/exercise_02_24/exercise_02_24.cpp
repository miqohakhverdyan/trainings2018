/// A program that checks the parity of the provided numbersi

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int 
main()
{ 
    int number; /// a variable to store the provided number
    std::cout << "Enter a number: "; /// prompts for data
    std::cin >> number; /// reads the integer and stores it in number

    if (number % 2 == 0) {
        std::cout << "Your number is even\n";
        return 0;
    }

    std::cout << "Your number is odd\n";
    return 0; /// indicates successful completion of the program
} /// end function main

