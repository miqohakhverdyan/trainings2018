1.cout << x; that will print 2
2.cout << x + x; that will print  4  
3.cout << "x="; that will print  x=
4.cout << "x = " << x;   that will print   x=2
5.cout << x + y << " = " << y + x;  that will print   5 = 5
6.z = x + y;  that willn't print anything   
7.cin >> x >> y; that willn't print anything
8.// cout << "x + y = " << x + y; that willn't print anything
9.cout << "\n"; empty line

