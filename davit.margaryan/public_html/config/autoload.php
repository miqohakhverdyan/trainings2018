<?php 

///////////////////////////////////////////////////////////////////
// 
// CONFIGURATION OF AUTOLOAD
//
// Automatically load all libraries and models
// from directories called libs and models.
//
///////////////////////////////////////////////////////////////////

require_once 'config/config.php';
// TODO: require db connectivity configs

/**
 * Autoload function which loads all classes. It uses namespaces
 * as a class directory path and loads all classes.
 *
 * @param $name - namespace\className 
 * @return void
 */
function __autoload($name) {
    $name[0] = strtolower($name[0]);
    $name = str_replace("\\", "/", $name);
    if (file_exists($name.'.php') && is_file($name.'.php')) {
        require_once $name.'.php';
    }
}
?>