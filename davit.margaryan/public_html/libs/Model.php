<?php

namespace Libs;

///////////////////////////////////////////////////////////////////
// 
// ABSTRACT CLASS MODEL
//
// Main model class allows program to perform connectivity
// to DB and whole bunch of methods working with DB
// 
//
///////////////////////////////////////////////////////////////////

abstract class Model
{
    
    function __construct()
    {
        // TODO: DB single object
    }

    abstract public function get($id = NULL);
}
?>