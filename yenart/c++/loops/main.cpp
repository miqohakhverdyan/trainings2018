#include <iostream>

#include "headers/SomeMeaningfulType.hpp"

int
f(const int size)
{
    int sum = 0;
    for (int i = 0; i < size; ++i) {
        const int i2 = i * i;
        sum += i2;
    }
    return sum;
}

int
main()
{
    int count;
    std::cout << "Input the count of the elements: ";
    std::cin  >> count;
    if (count > SomeMeaningfulType::SIZE) {
        std::cerr << "Error 1: The count should be less than " << SomeMeaningfulType::SIZE << std::endl;
        return 1;
    }

    const int sum1 = f(SomeMeaningfulType::SIZE);
    const int sum2 = f(count);

    return sum1 < sum2 ? 1 : 0;
}

